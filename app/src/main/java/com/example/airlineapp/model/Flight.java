package com.example.airlineapp.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.gson.annotations.SerializedName;

import java.time.LocalTime;

/**
 * This "Plain Old Java Object" (POJO) class defines a response for a
 * flight, which is returned by various microservices to indicate
 * which flights match a {@code FlightRequest}.
 */
public class Flight {
    /**
     * This field is the primary key of an {@link Flight} object.
     */
    Long id;

    /**
     * Airport code for the departing airport.
     */
    String departureAirport;

    /**
     * The date of the departure.
     */
    @SerializedName("departureDate")
    String departureDate;

    /**
     * The time of the departure.
     */
    @SerializedName("departureTime")
    String departureTime;

    /**
     * Airport code for the arriving airport.
     */
    String arrivalAirport;

    /**
     * The date of the arrival.
     */
    @SerializedName("arrivalDate")
    String arrivalDate;

    /**
     * The time of the arrival.
     */
    @SerializedName("arrivalTime")
    String arrivalTime;

    /**
     * The distance in kilometers from the departure to the arrival
     * airports.
     */
    int kilometers;

    /**
     * Price.
     */
    Double price;

    /**
     * The currency of the price.
     */
    String currency;

    /**
     * Airline code, e.g., "AA", "SWA", etc.
     */
    String airlineCode;

    /**
     * Default constructor needed for WebFlux param passing.
     */
    public Flight() {
    }

    /**
     * Constructor initializes the fields.
     //     * @param returnDepartureDateTime Return departure date and time
     //     * @param returnArrivalDateTime Return arrival date and time
     * @param departureAirport Departure airport
     * @param arrivalAirport Arrival airport
     * @param price Price of the flight leg
     * @param airlineCode Airline code (e.g., "SWA", "AA", etc.)
     */
    public Flight(String departureAirport,
                  String departureDate, String departureTime,
                  String arrivalAirport,
                  String arrivalDate, String arrivalTime,
                  Double price,
                  String airlineCode) {
        this.departureDate = departureDate;
        this.departureTime = departureTime;
        this.arrivalDate = arrivalDate;
        this.arrivalTime = arrivalTime;
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.price = price;
        this.airlineCode = airlineCode;
    }

    /**
     * @return Returns true if there's a match between this {@code
     * FlightResponse} and the {@code tripResponse} param
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public boolean equals(Object tripResponse) {
        if (tripResponse.getClass() != this.getClass())
            return false;

        Flight t = (Flight) tripResponse;
        return this.departureDate
                .equals(t.departureDate)
                && this.departureAirport
                .equals(t.departureAirport)
                && this.arrivalAirport
                .equals(t.arrivalAirport);
    }

    /**
     * @return A String representation of this {@code FlightResponse}
     */
    @Override
    public String toString() {
        return departureDate
                + ", "
                + arrivalDate
                + ", "
                + departureAirport
                + ", "
                + arrivalAirport
                + ", "
                + price
                + ", "
                + airlineCode;
    }

    /**
     * Get the departure date and time.
     */
    public String getDepartureDate() {
        return departureDate;
    }

    public String getDepartureTime() {
        return departureTime.substring(0,5);
    }

    /**
     * Set the departure date and time.
     */
    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public void setDepartureTime(LocalTime departureDate) {
        this.departureTime = departureTime;
    }

    /**
     * Get the arrival date and time.
     */
    public String getArrivalDate() {
        return arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime.substring(0,5);
    }

    /**
     * Set the arrival date and time.
     */
    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * Get the departure airport code.
     */
    public String getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Set the departure airport code.
     */
    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    /**
     * Get the arrival airport code.
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * Set the arrival airport code.
     */
    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    /**
     * Get the price for the flight leg.
     */
    public Double getPrice() {
        return price;
    }

    /**
     * Set the price for the flight leg.
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * Get the airpline code.
     */
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * Set the airpline code.
     */
    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    /**
     * Factory method that creates a new {@code Trip} object.
     * @param departureAirport Departure airport
     * @param arrivalAirport Arrival airport
     * @param price Price of the flight leg
     * @param airlineCode Airline code (e.g., "SWA", "AA", etc.)
     */
    public static Flight valueOf(String departureDate,
                                 String departureTime,
                                 String arrivalDate,
                                 String arrivalTime,
                                 String departureAirport,
                                 String arrivalAirport,
                                 Double price,
                                 String airlineCode) {
        return new Flight(departureAirport, departureDate, departureTime,
                arrivalAirport, arrivalDate, arrivalTime,
                price,
                airlineCode);
    }
}
