package com.example.airlineapp.viewmodels;

import android.accounts.NetworkErrorException;
import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.airlineapp.App;
import com.example.airlineapp.R;
import com.example.airlineapp.common.Resource;
import com.example.airlineapp.common.SingleLiveEvent;
import com.example.airlineapp.model.Airport;
import com.example.airlineapp.model.Flight;
import com.example.airlineapp.model.FlightRequest;
import com.example.airlineapp.web.RemoteDataSource;

import java.io.IOException;
import java.net.ConnectException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import timber.log.Timber;

/**
 * A shared ViewModel implemented via RxJava and used by all fragments
 * flight management fragments.
 */
public class MainViewModel extends BaseViewModel {
    /**
     * Keeps track of all outstanding RxJava requests so they can be
     * disposed in one fell swoop!
     */
    private final CompositeDisposable compositeDisposable =
        new CompositeDisposable();

    /**
     * Live data to post and observe API request results from server.
     */
    private final MutableLiveData<Resource<List<Airport>>> _airports =
        new MutableLiveData<>();

    private final MutableLiveData<Resource<List<Flight>>> _flights =
        new MutableLiveData<>();

    private final MutableLiveData<Resource<List<String>>> _dates =
        new MutableLiveData<>();

    /**
     * Live data bound to fragment widgets.
     */
    private final MutableLiveData<Airport> departFromAirport =
        new MutableLiveData<>();

    private final MutableLiveData<Airport> departToAirport =
        new MutableLiveData<>();

    private final MutableLiveData<Date> departureDate =
        new MutableLiveData<>();

    private final MutableLiveData<Date> returnDate =
        new MutableLiveData<>();

    private final MutableLiveData<String> departureTakeOffDateTime =
        new MutableLiveData<>();

    private final MutableLiveData<String> departureLandingDateTime =
        new MutableLiveData<>();

    private final MutableLiveData<String> returnTakeOffDateTime =
        new MutableLiveData<>();

    private final MutableLiveData<String> returnLandingDateTime =
        new MutableLiveData<>();

    private final MutableLiveData<Integer> passengers =
        new MutableLiveData<>(1);

    private final MutableLiveData<Double> departurePrice =
        new MutableLiveData<>();

    private final MutableLiveData<Double> returnPrice =
        new MutableLiveData<>();

    /**
     * Live data event used to navigate to authentication page.
     */
    private final SingleLiveEvent<Void> authenticate =
        new SingleLiveEvent<>();

    /**
     * Airports are posted and can be observed using this live data
     * object.
     */
    public final LiveData<Resource<List<Airport>>> airports =
        _airports;

    /**
     * Flights are posted and can be observed using this live data
     * object.
     */
    public final LiveData<Resource<List<Flight>>> flights =
        _flights;

    /**
     * Flight dates are posted and can be observed using this live
     * data object.
     */
    public final LiveData<Resource<List<String>>> dates =
        _dates;

    /**
     * Return the authenticate field.
     */
    public SingleLiveEvent<Void> getAuthenticate() {
        return authenticate;
    }

    /**
     * Injected data source used to make API calls to remote server.
     */
    @Inject
   RemoteDataSource dataSource;

    /**
     * Constructor calls Dagger application component to perform any
     * required member injections.
     */
    public MainViewModel(Application app) {
        super(app);
        ((App) app).getAppComponent().inject(this);
    }

    /**
     * Called when ViewModel is being destroyed.
     */
    @Override
    protected void onCleared() {
        // Dispose any pending RxJava requests.
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }

        super.onCleared();
    }

    /**
     * Clears all live data and disposes of any outstanding
     * RxJava requests.
     */
    private void clearData() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }

        _airports.setValue(null);
        _flights.setValue(null);
        _dates.setValue(null);

        departFromAirport.setValue(null);
        departToAirport.setValue(null);

        departureDate.setValue(null);
        returnDate.setValue(null);
        departureTakeOffDateTime.setValue(null);
        departureLandingDateTime.setValue(null);
        returnTakeOffDateTime.setValue(null);
        returnLandingDateTime.setValue(null);

        passengers.setValue(null);
        departurePrice.setValue(null);
        returnPrice.setValue(null);
    }

    /**
     * Asynchronously logout user from server.
     */
    public void logout() {
        // Asynchronously logout from remote session.
        Disposable subscription = dataSource
            // Log the client out.
            .logout()

            // Perform the logout on an I/O thread.
            .subscribeOn(Schedulers.io())

            // Process the results on the Android UI thread.
            .observeOn(AndroidSchedulers.mainThread())

            // Display success or failure in the UI thread.
            .subscribe(this::logoutSucceeded,
                       throwable -> Timber.e(throwable,
                                     "Logout request failed."));

        // Add this subscription to the items that will be disposed.
        compositeDisposable.add(subscription);
    }

    /**
     * Handles logout success.
     */
    private void logoutSucceeded() {
        // Clear all live events and RxJava requests.
        clearData();

        // Navigate to authentication page.
        authenticate.call();
    }

    /**
     * Asynchronously request a {@link List} of {@link Airport}
     * objects from the server.
     */
    public void requestAirports() {
        // Asynchronously make API request for list of known airports.
        Disposable subscription = dataSource
            // Get an Single that emits a List of Airport objects.
            .getAirports()

            // Get the Airport objects in the I/O thread pool.
            .subscribeOn(Schedulers.io())

            // Process the results on the Android UI thread.
            .observeOn(AndroidSchedulers.mainThread())

            // Display success or failure on the UI thread.
            .subscribe(this::airportsSuccess,
                       this::airportsError);

        // Add this subscription to the items that will be disposed.
        compositeDisposable.add(subscription);
    }

    /**
     * Handles airport request success by posting result to fragment
     * observer.
     *
     * @param airports List of airports.
     */
    private void airportsSuccess(List<Airport> airports) {
        _airports.setValue(Resource.success(airports));
    }

    /**
     * Handles unexpected airport request failures by posting error to
     * fragment observer. Also handles authentication failure by
     * attempting to re-authenticate user.
     *
     * @param throwable API error
     */
    private void airportsError(Throwable throwable) {
        String error = getApplication().getString(R.string.error_retrieve_airports);
        Timber.i("airportsError: %s", throwable.getClass());
        if (throwable instanceof ConnectException) {
            error = "Cannot connect to the server. Please check your network connection.";
        } else if (throwable instanceof NetworkErrorException) {
            error = "No network connection. Please check your network connection.";
        } else if (throwable instanceof IOException) {
            error = "No network connection. Please check your network connection.";
        } else if (throwable instanceof HttpException) {
            if (throwable.getMessage() != null && throwable.getMessage().contains("401")) {
                // Asynchronously attempt re-authentication.
                authenticate.call();
                return;
            } else {
                error = throwable.getMessage();
            }
        }
        _airports.setValue(Resource.error(error, null));
    }

    /**
     * Asynchronously request flights from the server
     *
     * @param flightRequest The user's flight request
     */
    public void requestFlights(FlightRequest flightRequest) {
        // Asynchronously make API call to receive all matching
        // flights.
        Disposable subscription = dataSource
            // Get an Single that emits a List of Flight objects.
            .findFlights(flightRequest)

            // Get the Flight objects in the I/O thread pool.
            .subscribeOn(Schedulers.io())

            // Process the results on the Android UI thread.
            .observeOn(AndroidSchedulers.mainThread())

            // Display success or failure in the UI thread.
            .subscribe(this::flightsSuccess,
                       this::flightsError);

        // Add this subscription to the items that will be disposed.
        compositeDisposable.add(subscription);
    }

    private void flightsSuccess(List<Flight> flights) {
        Timber.i("flightsSuccess: %s", flights.get(0).toString());
        _flights.setValue(Resource.success(flights));
    }

    /**
     * Handles unexpected flight request failures by posting error to
     * fragment observer.  Also handles authentication failure by
     * attempting to re-authenticate user.
     *
     * @param throwable API error
     */
    private void flightsError(Throwable throwable) {
        String error = getApplication().getString(R.string.error_retrieve_flights);
        Timber.i("flightsError: %s", throwable.toString());
        if (throwable instanceof ConnectException) {
            error = "Cannot connect to the server. Please check your network.";
        } else if (throwable instanceof NetworkErrorException) {
            error = "No network connection. Please check your network.";
        } else if (throwable instanceof IOException) {
            error = "No network connection. Please check your network.";
        } else if (throwable instanceof HttpException) {
            if (throwable.getMessage() != null && throwable.getMessage().contains("401")) {
                // Asynchronously attempt re-authentication.
                authenticate.call();
                return;
            } else {
                error = throwable.getMessage();
            }
        }
        _flights.setValue(Resource.error(error, null));
    }

    /**
     * Asynchronously request available dates from the server.
     *
     * @param departureAirport departure date
     * @param arrivalAirport arrival date
     */
    public void requestDepartureDates(String departureAirport,
                                      String arrivalAirport) {
        // Asynchronously make API call to find all matching flights.
        Disposable subscription = dataSource
            // Get an Single that emits a List of Strings containing
            // the departure dates.
            .getDepartureDates(departureAirport, arrivalAirport)

            // Get the Flight objects in the I/O thread pool.
            .subscribeOn(Schedulers.io())

            // Process the results on the Android UI thread.
            .observeOn(AndroidSchedulers.mainThread())

            // Display success or failure in the UI thread.
            .subscribe(this::datesSuccess,
                       this::datesError);

        // Add this subscription to the items that will be disposed.
        compositeDisposable.add(subscription);
    }

    /**
     * Set live data dates for fragment to observe.
     *
     * @param dates Dates returned from server.
     */
    private void datesSuccess(List<String> dates) {
        Timber.i("datesSuccess: %s", dates.size());
        _dates.setValue(Resource.success(dates));
    }

    /**
     * Handles unexpected date request failures by posting error to
     * fragment observer. Also handles authentication failure by
     * attempting to re-authenticate user.
     *
     * @param throwable API error.
     */
    private void datesError(Throwable throwable) {
        String error = getApplication().getString(R.string.error_retrieve_dates);
        Timber.i("datesError: %s", throwable.toString());
        if (throwable instanceof java.net.ConnectException) {
            error = "Cannot connect to the server. Please check your network.";
        } else if (throwable instanceof NetworkErrorException) {
            error = "No network connection. Please check your network.";
        } else if (throwable instanceof IOException) {
            error = "No network connection. Please check your network.";
        } else if (throwable instanceof HttpException) {
            if (throwable.getMessage() != null && throwable.getMessage().contains("401")) {
                // Asynchronously attempt re-authentication.
                authenticate.call();
                return;
            } else {
                error = throwable.getMessage();
            }
        }
        _dates.setValue(Resource.error(error, null));
    }

    /**
     * Navigates to departure fragment if all flight parameters have
     * been properly specified by user.
     */
    public void findFlights() {
        if (getDepartFromAirport().getValue() == null) {
            getNavigateError().postValue("From airport is required.");
        } else if (getDepartToAirport().getValue() == null) {
            getNavigateError().postValue("To airport is required.");
        } else if (getDepartureDate().getValue() == null) {
            getNavigateError().postValue("Depart date is required.");
        } else if (getReturnDate().getValue() == null) {
            getNavigateError().postValue("Return date is required.");
        } else {
            getNavigate().postValue(R.id.action_flightsFragment_to_departFragment);
        }
    }

    public LiveData<Airport> getDepartFromAirport() {
        return departFromAirport;
    }

    public void setDepartFromAirport(Airport airport) {
        departFromAirport.setValue(airport);
    }

    public LiveData<Airport> getDepartToAirport() {
        return departToAirport;
    }

    public void setDepartToAirport(Airport airport) {
        departToAirport.setValue(airport);
    }

    //
    // ACCESSORS REQUIRED FOR DATA BINDING.
    //
    public LiveData<Date> getDepartureDate() {
        return departureDate;
    }

    public LiveData<Date> getReturnDate() {
        return returnDate;
    }

    public LiveData<String> getDepartureTakeOffDateTime() {
        return departureTakeOffDateTime;
    }

    public LiveData<String> getDepartureLandingDateTime() {
        return departureLandingDateTime;
    }

    public LiveData<String> getReturnTakeOffDateTime() {
        return returnTakeOffDateTime;
    }

    public LiveData<String> getReturnLandingDateTime() {
        return returnLandingDateTime;
    }

    public void setDepartureTakeOffDateTime(String time) {
        departureTakeOffDateTime.setValue(time);
    }

    public void setDepartureLandingDateTime(String time) {
        departureLandingDateTime.setValue(time);
    }

    public void setReturnTakeOffDateTime(String time) {
        returnTakeOffDateTime.setValue(time);
    }

    public void setReturnLandingDateTime(String time) {
        returnLandingDateTime.setValue(time);
    }

    public String getFormattedDepartureDate() {
        return formatDate(departureDate.getValue());
    }

    public String getFormattedReturnDate() {
        return formatDate(returnDate.getValue());
    }

    public void setDepartureDate(Date date) {
        departureDate.setValue(date);
    }

    public void setReturnDate(Date date) {
        returnDate.setValue(date);
    }

    public String formatDate(@Nullable Date date) {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd", Locale.getDefault());
        return date != null ? dateFormat.format(date) : "";
    }

    public LiveData<Integer> getPassengers() {
        return passengers;
    }

    public void updatePassengers(int addValue) {
        passengers.postValue(Math.max(0, passengers.getValue() + addValue));
    }

    public Double getDeparturePrice() {
        return departurePrice.getValue();
    }

    public void setDeparturePrice(Double price) {
        departurePrice.setValue(price);
    }

    public Double getReturnPrice() {
        return returnPrice.getValue();
    }

    public void setReturnPrice(Double price) {
        returnPrice.setValue(price);
    }
}
