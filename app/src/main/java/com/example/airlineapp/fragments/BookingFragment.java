package com.example.airlineapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.airlineapp.R;
import com.example.airlineapp.databinding.BookingFragmentBinding;
import com.example.airlineapp.viewmodels.MainViewModel;

import org.jetbrains.annotations.NotNull;

public class BookingFragment extends Fragment {

    private MainViewModel viewModel;
    private BookingFragmentBinding binding;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.booking_fragment,
                container,
                false);

        // set the ViewModel for the depart fragment
        viewModel = (new ViewModelProvider(requireActivity())).get(MainViewModel.class);
        binding.setViewModel(viewModel);
        binding.setFragment(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String departureTime = getArguments().getString("departureTime");
        String arrivalTime = getArguments().getString("arrivalTime");
        Double price = getArguments().getDouble("price");
        viewModel.setReturnTakeOffDateTime(departureTime);
        viewModel.setReturnLandingDateTime(arrivalTime);
        viewModel.setReturnPrice(price);

        double totalPrice = viewModel.getDeparturePrice() + viewModel.getReturnPrice();

        //TODO: currency type should be supported.
        binding.tvPrice.setText("$" + totalPrice);
    }

    public void onReserveClick() {
        Toast.makeText(getContext(),
                "You have reached the end of this mock application!",
                Toast.LENGTH_LONG).show();
    }
}