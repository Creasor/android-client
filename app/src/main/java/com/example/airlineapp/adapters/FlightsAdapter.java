package com.example.airlineapp.adapters;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

//import androidx.databinding.library.baseAdapters.BR;
import com.example.airlineapp.R;
import com.example.airlineapp.model.Flight;

import java.util.List;

public class FlightsAdapter extends RecyclerView.Adapter<FlightsAdapter.ViewHolder> {

    public static final String TAG = "PostsAdapter";

    private ViewDataBinding binding;
    private List<Flight> flights;

    // Provide a suitable constructor
    public FlightsAdapter(List<Flight> flights) {
        this.flights = flights;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_flight, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(binding);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Flight flight = flights.get(position);
        holder.bind(flight);
    }

    @Override
    public int getItemCount() {
        return flights.size();
    }

    public void clear() {
        flights.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Flight> flights) {
        this.flights.addAll(flights);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Flight flight) {
            binding.getRoot().setOnClickListener(v -> {
                // pass information to the next fragment
                Bundle bundle = new Bundle();
                bundle.putSerializable("arrivalTime", flight.getArrivalTime());
                bundle.putSerializable("departureTime", flight.getDepartureTime());
                bundle.putDouble("price", flight.getPrice());

                // move to next fragment on click on the flight
                Navigation.findNavController(v)
                        .navigate(R.id.action_currentFragment_to_nextFragment,
                                bundle);
            });
            binding.setVariable(BR.flight, flight);
            binding.executePendingBindings();
        }
    }
}
