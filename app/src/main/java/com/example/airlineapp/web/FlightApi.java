package com.example.airlineapp.web;

import com.example.airlineapp.model.Airport;
import com.example.airlineapp.model.Flight;

import java.time.LocalDate;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * This interface provides methods for each API end point for the
 * remote Flight Listing App (FLApp) server. The Retrofit instance
 * built from this interface has full RxJava asynchronous support for
 * each API method.
 *
 * The {@code @POST} annotation is a request method designator and
 * corresponds to the similarly named HTTP method.  The Java method
 * annotated with this request method designator will process HTTP
 * POST requests.  The behavior of a resource is determined by the
 * HTTP method to which the resource is responding.
 *
 * The {@code @FormUrlEncoded} annotation denotes that the request
 * body will use form URL encoding, in which case fields should be
 * declared as parameters and annotated with {@code @Field}.
 * 
 * The {@code @GET} annotation is a request method designator and
 * corresponds to the similarly named HTTP method.  The Java method
 * annotated with this request method designator will process HTTP GET
 * requests.  The behavior of a resource is determined by the HTTP
 * method to which the resource is responding.
 *
 * The {@code @Query} annotation denotes a parameter appended to the
 * URL.
 */
public interface FlightApi {
    /**
     * Path for the OAuth endpoint.
     */
    String TOKEN_PATH = "/oauth/token";

    /**
     * Path for the logout endpoint.
     */
    String LOGOUT_PATH = "/logout";

    /**
     * Path for the airports endpoint.
     */
    String AIRPORTS_PATH = "/airports";

    /**
     * Path for the dates endpoint.
     */
    String DATES_PATH = "/dates";

    /**
     * Path for the flights endpoint.
     */
    String FLIGHTS_PATH = "/flights";

    /**
     * Token request end point.
     *
     * @param clientId Client ID for this application.
     * @param grantType PKCE grant type
     * @param redirectUri Authorization redirect URL
     * @param code Code provided by server authorization response.
     * @param state Random state string for verifying responses.
     * @param scope Authorization scope
     * @param codeVerifier Code verifier matching a previous sent code challenge.
     * @return The token response containing access and refresh tokens.
     */
    @FormUrlEncoded
    @POST(TOKEN_PATH)
    Single<TokenResponse> requestToken(@Field("client_id") String clientId,
                                       @Field("grant_type") String grantType,
                                       @Field("redirect_uri") String redirectUri,
                                       @Field("code") String code,
                                       @Field("state") String state,
                                       @Field("scope") String scope,
                                       @Field("code_verifier") String codeVerifier);

    /**
     * Refresh token end point.
     *
     * @param clientId Client ID for this application.
     * @param grantType PKCE grant type
     * @param scope Authorization scope
     * @param refreshToken Server supplied refresh token.
     * @return The token response containing access and refresh tokens.
     */
    @FormUrlEncoded
    @POST(TOKEN_PATH)
    Single<TokenResponse> refreshToken(@Field("client_id") String clientId,
                                       @Field("grant_type") String grantType,
                                       @Field("scope") String scope,
                                       @Field("refresh_token") String refreshToken);

    /**
     * Finds all known airports.
     *
     * @return A {@link Single} that emits a {@link List} of {@code
     * Airport} objects
     */
    @GET(AIRPORTS_PATH)
    Single<List<Airport>> getAirports();

    /**
     * Finds all departure dates that have at least one flight running
     * from the departure airport to the arrival airport.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link Single} that emits a {@link List} of all matching dates
     */
    @GET(DATES_PATH)
    Single<List<String>> getDepartureDates(@Query("departureAirport") String departureAirport,
                                           @Query("arrivalAirport") String arrivalAirport);

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to the desired
     * {@code currency}.
     *
     * @param departureAirport The departure airport
     * @param departureDate The departure date
     * @param arrivalAirport   The arrival airport
     * @param currency The desired currency
     * @return A {@link List} of matching {@link Flight} objects in
     * the desired currency
     */
    @GET(FLIGHTS_PATH)
    Single<List<Flight>> findFlights(@Query("departureAirport") String departureAirport,
                                     @Query("arrivalAirport") String arrivalAirport,
                                     @Query("departureDate") LocalDate departureDate,
                                     @Query("passengers") int passengers,
                                     @Query("currency") String currency);

    /**
     * Provides support for logging out of an active remote server
     * session.
     *
     * @param clientId The client ID of this application.
     * @return A RxJava {@link Completable} to subscribe to
     */
    @FormUrlEncoded
    @POST(LOGOUT_PATH)
    Completable logout(@Field("client_id") String clientId);
}
