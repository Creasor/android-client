package com.example.airlineapp;//package com.example.airlineapp;
//import android.os.Build;
//
//import androidx.annotation.RequiresApi;
//
//import com.example.airlineapp.model.Airport;
//import com.example.airlineapp.model.Flight;
//import com.example.airlineapp.model.FlightRequest;
//
//import java.time.LocalDateTime;
//import java.util.List;
//import java.util.stream.Stream;
//
//import java.util.function.Predicate;
//
//import static java.util.stream.Collectors.toList;
//
///**
// * This utility class contains methods that obtain test data.
// */
//public class TestDataFactory {
//    /**
//     * A utility class should always define a private constructor.
//     */
//    private TestDataFactory() {
//    }
//
//    private static String[] sFlightInfo = {
//            // Feel free to add more rows to this array to generate more test data!
//            "2021-03-20T07:00:00,2021-03-20T10:00:00," +
//                    "LHR,JFK,777.00,AA",
//            "2021-03-25T19:00:00,2021-03-25T10:00:00," +
//                    "JFK,LHR,555.00,AA",
//            "2021-03-20T08:00:00,2021-03-20T10:00:00," +
//                    "LHR,JFK,888.00,SWA",
//            "2021-03-25T19:00:00,2021-03-25T10:00:00," +
//                    "JFK,LHR,666.00,SWA"
//    };
//
//    private static String[] sAirportsInfo = {
//            "Akron/Canton, OH  - CAK",
//            "Albany, NY  - ALB",
//            "Albuquerque, NM  - ABQ",
//            "Allentown, PA  - ABE",
//            "Amarillo, TX  - AMA",
//            "Anchorage, AK  - ANC",
//            "Asheville, NC  - AVL",
//            "Aspen, CO  - ASE",
//            "Athens, GA  - AHN",
//            "Atlanta, GA  - ATL",
//            "Atlantic City, NJ  - AIY",
//            "Austin, TX  - AUS",
//            "Baltimore, MD  - BWI",
//            "Baton Rouge, LA  - BTR",
//            "Bethlehem, PA  - ABE",
//            "Birmingham, AL  - BHM",
//            "Bloomington, IL  - BMI",
//            "Boise, ID  - BOI",
//            "Boston, MA  - BOS",
//            "Boulder, CO  - WBU",
//            "Bowling Green, KY  - BWG",
//            "Bozeman, MT  - BZN",
//            "Burlington, VT  - BTV",
//            "Butte, MT  - BTM",
//            "Carlsbad, CA  - CLD",
//            "Cedar Rapids, IA  - CID",
//            "Champaign/Urbana, IL  - CMI",
//            "Charleston, SC  - CHS",
//            "Charlotte, NC  - CLT",
//            "Charlottesville, VA  - CHO",
//            "Chattanooga, TN  - CHA",
//            "Chicago, IL (Midway) - MDW",
//            "Chicago, IL (O'Hare) - ORD",
//            "Cincinnati, OH  - CVG",
//            "Clearwater/St Petersburg, FL, USA - PIE",
//            "Cleveland, OH, USA - CLE",
//            "Columbus, OH, USA - CMH",
//            "Dallas/Fort Worth, TX, USA - DFW",
//            "Dayton, OH, USA - DAY",
//            "Denver, CO, USA - DEN",
//            "Detroit, MI, USA - DTW",
//            "Raleigh/Durham, NC, USA - RDU",
//            "El Paso, TX, USA - ELP",
//            "Fayetteville, AR, USA - FYV",
//            "Flagstaff, AZ, USA - FLG",
//            "Fort Lauderdale, FL, USA - FLL",
//            "Gary, IN, USA - GYY",
//            "Green Bay, WI, USA - GRB",
//            "Greensboro, NC, USA - GSO",
//            "Harrisburg, PA, USA - MDT",
//            "Houston, TX (Hobby), USA - HOU",
//            "Huntsville, AL, USA - HSV",
//            "Indianapolis, IN, USA - IND",
//            "Jackson Hole, WY, USA - JAC",
//            "Kahului, HI, USA - OGG",
//            "Kalamazoo, MI, USA - AZO",
//            "Kansas City, MO, USA - MCI",
//            "Key West, FL, USA - EYW",
//            "Lancaster, PA, USA - LNS",
//            "Las Vegas, NV, USA - LAS",
//            "Lincoln, NE, USA - LNK",
//            "London/Heathrow, UK - LHR",
//            "Long Beach, CA, USA - LGB",
//            "Los Angeles, CA, USA - LAX",
//            "Madison, WI, USA - MSN",
//            "Miami, FL, USA - MIA",
//            "Minneapolis, MN, USA - MSP",
//            "Mobile, AL, USA - MOB",
//            "Muscle Shoals, AL, USA - MSL",
//            "Nashville, TN, USA - BNA",
//            "New Orleans, LA, USA - MSY",
//            "New York, NY (Kennedy), USA - JFK",
//            "New York, NY (La Guardia), USA - LGA",
//            "Newark, NJ, USA - EWR",
//            "Norfolk, VA, USA - ORF"
//    };
//
//    /**
//     * Return a list of {@code TripResponse} objects that match the
//     * given {@code tripRequest}.
//     */
//    @RequiresApi(api = Build.VERSION_CODES.N)
//    public static List<Flight> findFlights(FlightRequest tripRequest) {
//        return Stream
//                // Convert the array into a stream.
//                .of(sFlightInfo)
//
//                // Convert the strings into TripResponse objects.
//                .map(TestDataFactory::makeTrip)
//
//                // Only keep TripResponse objects that match the
//                // tripRequest.
//                .filter(tripRequest::equals)
//
//                // Collect the results into a list.
//                .collect(toList());
//    }
//
//    /**
//     * Factory method that converts a string of comma-separated values
//     * indicating the date/time of the initial departure/arrival, the
//     * date/time of the return departure/arrival, the airport code for
//     * the departing/arriving airports, the price, and the airline
//     * code into the corresponding {@code TripResponse}.
//     *
//     * @param tripString A string containing comma-separated values
//     * indicating information about the trip
//     * @return The corresponding {@code TripResponse}
//     */
//    @RequiresApi(api = Build.VERSION_CODES.O)
//    private static Flight makeTrip(String tripString) {
//        String[] result = tripString.split(",");
//
//        // Create and return a TripResponse via a factory method.
//        return Flight
//                .valueOf(// Date/time of the initial departure.
//                        LocalDateTime.parse(result[0]),
//                        // Date/time of the initial arrival.
//                        LocalDateTime.parse(result[1]),
//
//                        // Code for the departure airport.
//                        result[2],
//
//                        // Code for the arrival airport.
//                        result[3],
//
//                        // Price of the flight.
//                        Double.parseDouble(result[4]),
//
//                        // Code for the airline.
//                        result[5]);
//    }
//
//    /**
//     * @return The contents in the {@code filename} as a list of
//     * non-empty {@code AirportInfo} objects.
//     */
//    @RequiresApi(api = Build.VERSION_CODES.O)
//    public static List<Airport> getAirportInfoList() {
//        try {
////            return Files
////                    // Read all lines from filename and convert into a
////                    // stream of strings.
//////                    .lines(Paths.get(ClassLoader.getSystemResource
//////                            (filename).toURI()))
////                    .lines(Paths.get(filename))
////
////                    // Filter out any empty strings.
////                    .filter(((Predicate<String>) String::isEmpty).negate())
////
////                    // Convert the strings into AirportInfo objects.
////                    .map(TestDataFactory::makeAirportInfo)
////
////                    // Trigger intermediate operations and collect the
////                    // results into a list of AirportInfo objects.
////                    .collect(toList());
//            return Stream
//                    // Convert the array into a stream.
//                    .of(sAirportsInfo)
//
//                    // Only keep TripResponse objects that match the
//                    // tripRequest.
//                    .filter(((Predicate<String>) String::isEmpty).negate())
//
//                    // Convert the strings into TripResponse objects.
//                    .map(TestDataFactory::makeAirportInfo)
//
//                    // Collect the results into a list.
//                    .collect(toList());
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * Factory method that converts a string of hyphen-separated
//     * values into the corresponding {@code AirportInfo}.
//     *
//     * @param airportInfo A string containing comma-separated values
//     * indicating information about the trip
//     * @return The corresponding {@code AirportInfo} object
//     */
//    private static Airport makeAirportInfo(String airportInfo) {
//        // Split the airportInfo string via the hyphen ('-')
//        // character.
//        String[] result = airportInfo.split("-");
//
//        // Create and return an AirportInfo via a factory method.
//        return Airport
//                .valueOf(result[1],
//                        result[0]);
//    }
//
//}
