package com.example.airlineapp.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.airlineapp.R;
import com.example.airlineapp.adapters.FlightsAdapter;
import com.example.airlineapp.databinding.DepartFragmentBinding;
import com.example.airlineapp.model.Airport;
import com.example.airlineapp.model.FlightRequest;
import com.example.airlineapp.viewmodels.MainViewModel;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import timber.log.Timber;

/**
 * Fragment for depart flights
 */
@RequiresApi(api = Build.VERSION_CODES.O)
public class DepartFragment extends Fragment {

    private MainViewModel viewModel;
    private DepartFragmentBinding binding;
    private FlightsAdapter adapter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.depart_fragment,
                container,
                false);

        // set the ViewModel for the depart fragment
        viewModel = (new ViewModelProvider(requireActivity())).get(MainViewModel.class);
        binding.setViewModel(viewModel);
        binding.setFragment(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swapDepartReturn();

        adapter = new FlightsAdapter(new ArrayList<>());
        binding.rvFlights.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.rvFlights.setLayoutManager(layoutManager);
        Airport fromAirport = Objects.requireNonNull(viewModel.getDepartFromAirport().getValue());
        Airport toAirport = Objects.requireNonNull(viewModel.getDepartToAirport().getValue());
        binding.tvAirport1.setText(fromAirport.getAirportCode());
        binding.tvAirport2.setText(toAirport.getAirportCode());

        setupObservers();

        queryFlights();

        swapDepartReturn();

        if (getArguments() != null) {
            String departureTime = (String) getArguments()
                    .getSerializable("departureTime");
            String arrivalTime = (String) getArguments()
                    .getSerializable("arrivalTime");
            Double price = getArguments().getDouble("price");
            viewModel.setDepartureTakeOffDateTime(departureTime);
            viewModel.setDepartureLandingDateTime(arrivalTime);
            viewModel.setDeparturePrice(price);
        }
    }

    /**
     * Swap the departure and arrival airports when move to the returnFragment
     */
    public void swapDepartReturn() {
        String currentFragment =
                Objects.requireNonNull(Navigation.findNavController(requireView())
                        .getCurrentDestination())
                        .toString();
        if (currentFragment.contains("returnFragment")) {
            Airport toAirport = viewModel.getDepartToAirport().getValue();
            viewModel.setDepartToAirport(viewModel.getDepartFromAirport().getValue());
            viewModel.setDepartFromAirport(toAirport);
            Date returnDate = viewModel.getReturnDate().getValue();
            viewModel.setReturnDate(viewModel.getDepartureDate().getValue());
            viewModel.setDepartureDate(returnDate);
            binding.tvDepartLabel.setText(R.string.Return);
        }
    }

    /**
     * Create FlightRequest and query flights from the server
     */
    public void queryFlights() {
        Timber.i("queryFlights: %s",
                viewModel.getDepartFromAirport().getValue());
        Timber.i("queryFlights: %s",
                viewModel.getDepartToAirport().getValue());

        FlightRequest.Builder builder = FlightRequest.builder()
                .setDepartureDate(LocalDate.of(2021, 3, 14))
                .setCurrency("USD")
                .setPassengers(1);

        if (getArguments() != null) {
            builder.setDepartureAirport("2").setArrivalAirport("1");
        } else {
            builder.setDepartureAirport("1").setArrivalAirport("2");
        }

        viewModel.requestFlights(builder.build());
    }

    /**
     * Get the list of flights from the server;
     * add flights to the page onSuccess;
     * throw a toast to the user onError.
     */
    public void setupObservers() {
        viewModel.flights.observe(getViewLifecycleOwner(), resource -> {
            switch (resource.status) {
                case SUCCESS:
                    adapter.clear();
                    adapter.addAll(resource.data);
                    break;
                case ERROR:
                    Toast.makeText(getContext(), resource.message, Toast.LENGTH_SHORT).show();
                    break;
                case LOADING:
                    Toast.makeText(getContext(), "loading", Toast.LENGTH_SHORT)
                            .show();
                    break;
            }
        });
    }
}