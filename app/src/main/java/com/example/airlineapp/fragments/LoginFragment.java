package com.example.airlineapp.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.airlineapp.R;
import com.example.airlineapp.databinding.LoginFragmentBinding;
import com.example.airlineapp.viewmodels.LoginViewModel;

import org.jetbrains.annotations.NotNull;

public class LoginFragment extends Fragment {

    private LoginViewModel viewModel;
    private LoginFragmentBinding binding;

    @Override
    public View onCreateView(
            @NotNull LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.login_fragment,
                container,
                false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);
        setupViews();
        setupObservers();
        setupWebViewClient();
        setupWebView();
        binding.webView.loadUrl(viewModel.buildLoginUrl());
    }

    private void setupViews() {
        binding.swipeRefresh.setOnRefreshListener(this::retry);
    }

    private void retry() {
        binding.swipeRefresh.setRefreshing(false);
        String url = viewModel.buildLoginUrl();
        binding.webView.loadUrl(url);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void setupObservers() {
        viewModel.getUrlFeed().observe(
                getViewLifecycleOwner(),
                url -> binding.webView.loadUrl(url));

        viewModel.getLoginResult().observe(
                getViewLifecycleOwner(),
                result -> {
                    switch (result.status) {
                        case SUCCESS:
                            Navigation.findNavController(requireView())
                                    .navigate(R.id.action_loginFragment_to_flightsFragment);
                            break;
                        case ERROR:
                            showError(result.message);
                            retry();
                            break;
                        case LOADING:
                            break;
                    }

                }
        );
    }

    private void showError(String message) {
        Toast.makeText(binding.webView.getContext(), message, Toast.LENGTH_LONG).show();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWebView() {
        WebView.setWebContentsDebuggingEnabled(true);

        WebSettings settings = binding.webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setDatabaseEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setSafeBrowsingEnabled(false);
    }

    private void setupWebViewClient() {
        // Setup WebViewClient to handle redirect url containing OAuth PKCE code.
        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if (viewModel.handleUrlRedirect(request)) {
                    return true;
                } else {
                    return super.shouldOverrideUrlLoading(view, request);
                }
            }
        });
    }
}