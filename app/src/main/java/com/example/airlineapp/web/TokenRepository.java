package com.example.airlineapp.web;

import org.jetbrains.annotations.Nullable;

/**
 * A repository class for caching a server provided token.
 */
public class TokenRepository {
    /**
     * A token cache implementation.
     */
    private final TokenCache tokenCache;

    /**
     * Constructs this class using an injected token cache
     * implementation.
     *
     * @param tokenCache An injected token cache implementation
     */
    public TokenRepository(TokenCache tokenCache) {
        this.tokenCache = tokenCache;
    }

    /**
     * @return A cached token response object.
     */
    @Nullable TokenResponse getTokenResponse() {
        return tokenCache.get();
    }

    /**
     * @return An access token string for the currently
     * cached token response object.
     */
    @Nullable
    String getAccessToken() {
        TokenResponse tokenResponse = getTokenResponse();
        return tokenResponse != null ? tokenResponse.getAccessToken() : null;
    }

    /**
     * @return The refresh token contained in the
     * currently cached token response object.
     */
    @Nullable
    String getRefreshToken() {
        TokenResponse tokenResponse = getTokenResponse();
        return tokenResponse != null ? tokenResponse.getRefreshToken() : null;
    }

    /**
     * Caches the passed token response object.
     * @param tokenResponse Token response sent from server.
     */
    public void save(TokenResponse tokenResponse) {
        tokenCache.save(tokenResponse);
    }

    /**
     * Deletes any cached token response from this repository.
     */
    public void delete() {
        tokenCache.delete();
    }

    /**
     * Builds an authorization header using the access token
     * contained in the cached token response object.
     * @return An authorization header String.
     */
    public String getAuthorizationHeader() {
        TokenResponse tokenResponse = getTokenResponse();
        return tokenResponse != null
            ? tokenResponse.getTokenType()
            + " "
            + tokenResponse.getAccessToken()
            : "";
    }
}
