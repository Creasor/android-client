package com.example.airlineapp;

import android.app.Application;
import android.content.Context;

import com.example.airlineapp.di.AppComponent;
import com.example.airlineapp.di.AppModule;
import com.example.airlineapp.di.DaggerAppComponent;

import timber.log.Timber;

public class App extends Application {
    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent =
                DaggerAppComponent
                        .builder()
                        .appModule(new AppModule(this))
                        .build();
        Timber.plant(new Timber.DebugTree());
    }

    public static App getApp(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public static AppComponent getAppComponent(Context context) {
        return getApp(context).getAppComponent();
    }
}