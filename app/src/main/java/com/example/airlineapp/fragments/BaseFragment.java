package com.example.airlineapp.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.airlineapp.viewmodels.MainViewModel;

/**
 * Fragment that observers navigation events from BaseViewModel class.
 * The BaseViewModel controls all navigation events allowing fragments
 * to be completely decoupled from navigation logic.
 */
public class BaseFragment extends Fragment {
    protected MainViewModel viewModel;
    protected NavController navController;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = (new ViewModelProvider(requireActivity())).get(MainViewModel.class);
    }

    @Override
    public void onViewCreated(
            @NonNull View view,
            @Nullable Bundle savedInstanceState
    ) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(requireView());
        setupObservers();
    }

    private void setupObservers() {
        viewModel.getNavigateError().observe(getViewLifecycleOwner(),
                error -> Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show());

        viewModel.getNavigate().observe(getViewLifecycleOwner(),
                id -> {
                    if (id > 0) {
                        navController.navigate(id);
                    } else {
                        navController.navigateUp();
                    }
                });
    }
}
