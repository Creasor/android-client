package com.example.airlineapp.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.airlineapp.common.SingleLiveEvent;

import org.jetbrains.annotations.NotNull;

/**
 * A common base view model used to centralize navigation and error handling.
 */
public class BaseViewModel extends AndroidViewModel {
    /**
     * All navigation actions are controlled by this single live event.
     */
    private final SingleLiveEvent<Integer> navigate = new SingleLiveEvent<>();

    /**
     * All navigation errors are emitted by this single live event.
     */
    private final SingleLiveEvent<String> navigateError = new SingleLiveEvent<>();

    public BaseViewModel(@NonNull @NotNull Application application) {
        super(application);
    }

    /**
     * Accessor for navigation single live event.
     */
    public SingleLiveEvent<Integer> getNavigate() {
        return navigate;
    }

    /**
     * Accessor for navigation error single live event.
     */
    public SingleLiveEvent<String> getNavigateError() {
        return navigateError;
    }

    /**
     * Not currenty used.
     */
    private void navigateTo(int id) {
        navigate.postValue(id);
    }

    /**
     * Not currenty used.
     */
    protected void navigateUp() {
        navigate.postValue(0);
    }
}
