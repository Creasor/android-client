package com.example.airlineapp.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.time.LocalDate;

/**
 * Flight request data transfer object (DTO).
 */
public class FlightRequest {
    /**
     * Date and time of the departure.
     */
    LocalDate departureDate;

    /**
     * Airport code for the departing airport.
     */
    String departureAirport;

    /**
     * Airport code for the arriving airport.
     */
    String arrivalAirport;

    /**
     * Number of passengers.
     */
    int passengers;

    /**
     * Preferred currency for Flight price.
     */
    String currency;

    /**
     * Default constructor needed for WebFlux param passing.
     */
    public FlightRequest() {
    }

    /**
     * All args constructor only used by builder.
     */
    private FlightRequest(
            LocalDate departureDate,
            String departureAirport,
            String arrivalAirport,
            int passengers,
            String currency) {
        this.departureDate = departureDate;
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.passengers = passengers;
        this.currency = currency;
    }

    /**
     * @return Returns true if there's a match between this {@code
     * FlightRequest} and the {@code tripRequest} param
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public boolean equals(Object tripRequest) {
        if (tripRequest.getClass() != this.getClass())
            return false;

        FlightRequest t = (FlightRequest) tripRequest;
        return this.departureDate
                .equals(t.departureDate)
                && this.departureAirport
                .equals(t.departureAirport)
                && this.arrivalAirport
                .equals(t.arrivalAirport);
    }

    /**
     * @return Returns true if there's a match between this {@link
     * FlightRequest} and the {@code tripResponse} param
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean equals(Flight tripResponse) {
        return this.departureDate
                .equals(tripResponse.departureDate)
                && this.departureAirport
                .equals(tripResponse.departureAirport)
                && this.arrivalAirport
                .equals(tripResponse.arrivalAirport);
    }

    /**
     * The number of passengers (seats) required for any flight.
     */
    public int getPassengers() {
        return passengers;
    }

    /**
     * The desired currency for all request response pricing.
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Gets the departure date and time.
     */
    public LocalDate getDepartureDate() {
        return departureDate;
    }

    /**
     * Gets the departure airport code.
     */
    public String getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Gets the arrival airport code.
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * @return A String representation of this {@code FlightRequest}
     */
    @Override
    public String toString() {
        return departureDate
                + ", "
                + departureAirport
                + ", "
                + arrivalAirport
                + ", "
                + passengers;
    }

    public static FlightRequest.Builder builder() {
        return new FlightRequest.Builder();
    }

    /**
     * Factory builder that creates a new {@link FlightRequest}
     * object.
     */
    public static class Builder {
        /**
         * Date and time of the departure.
         */
        private LocalDate departureDate;

        /**
         * Airport code for the departing airport.
         */
        private String departureAirport;

        /**
         * Airport code for the arriving airport.
         */
        private String arrivalAirport;


        /**
         * Number of passengers.
         */
        private int passengers;

        /**
         * Preferred currency for Flight price.
         */
        private String currency;

        /**
         * Sets the departure airport code.
         */
        public Builder setDepartureAirport(String departureAirport) {
            this.departureAirport = departureAirport;
            return this;
        }

        /**
         * Sets the departure date.
         */
        public Builder setDepartureDate(LocalDate departureDate) {
            this.departureDate = departureDate;
            return this;
        }

        /**
         * Sets the arrival airport.
         */
        public Builder setArrivalAirport(String arrivalAirport) {
            this.arrivalAirport = arrivalAirport;
            return this;
        }

        /**
         * Sets the number of passengers.
         */
        public Builder setPassengers(int passengers) {
            this.passengers = passengers;
            return this;
        }

        /**
         * Sets the currency.
         */
        public Builder setCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        /**
         * Build the {@link FlightRequest}.
         */
        public FlightRequest build() {
            return new FlightRequest(departureDate,
                                     departureAirport,
                                     arrivalAirport,
                                     passengers,
                                     currency);
        }
    }
}
