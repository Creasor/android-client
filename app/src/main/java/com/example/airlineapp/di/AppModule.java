package com.example.airlineapp.di;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.airlineapp.App;
import com.example.airlineapp.web.RemoteDataSource;
import com.example.airlineapp.web.SharedPrefTokenCache;
import com.example.airlineapp.web.TokenCache;
import com.example.airlineapp.web.TokenRepository;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    final private Context applicationContext;

    public AppModule(Context context) {
        this.applicationContext = context.getApplicationContext();
    }

    @Provides
    @Singleton
    Context provideAppContext() {
        return applicationContext;
    }

    @Provides
    @Singleton
    App provideApp() {
        return (App) provideAppContext();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(applicationContext);
    }

    @Provides
    @Singleton
    TokenCache provideSharedPrefTokenCache(SharedPreferences sharedPreferences) {
        return new SharedPrefTokenCache(sharedPreferences);
    }

    @Provides
    @Singleton
    TokenRepository provideTokenRepository(TokenCache tokenCache) {
        return new TokenRepository(tokenCache);
    }

    @Provides
    @Singleton
    RemoteDataSource provideRemoteDataSource(TokenRepository tokenRepository) {
        return new RemoteDataSource(tokenRepository);
    }
}
