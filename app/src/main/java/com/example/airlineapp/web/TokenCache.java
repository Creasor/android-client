package com.example.airlineapp.web;

/**
 * Token cache interface.
 */
public interface TokenCache {
    /**
     * @return True iff a token exists in this cache.
     */
    boolean exists();

    /**
     * @return A previously cached token.
     */
    TokenResponse get();

    /**
     * Saves the passed tokenResonse in the cache.
     * @param tokenResponse A token response object.
     */
    void save(TokenResponse tokenResponse);

    /**
     * Deletes any cached token.
     */
    void delete();
}