package com.example.airlineapp.di;

import com.example.airlineapp.viewmodels.LoginViewModel;
import com.example.airlineapp.viewmodels.MainViewModel;
import com.example.airlineapp.web.RemoteDataSource;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(MainViewModel mainViewModel);

    void inject(LoginViewModel loginViewModel);

    void inject(RemoteDataSource remoteDataSource);
}