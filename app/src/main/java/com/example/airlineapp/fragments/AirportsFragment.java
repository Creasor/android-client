package com.example.airlineapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.airlineapp.R;
import com.example.airlineapp.databinding.AirportsFragmentBinding;
import com.example.airlineapp.model.Airport;
import com.example.airlineapp.viewmodels.MainViewModel;

public class AirportsFragment extends Fragment {

    private MainViewModel viewModel;
    private AirportsFragmentBinding binding;
    ArrayAdapter<Airport> adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.airports_fragment,
                container,
                false);
        // set the ViewModel for the depart fragment
        viewModel = (new ViewModelProvider(requireActivity())).get(MainViewModel.class);
        binding.setViewModel(viewModel);
        binding.setFragment(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupListeners();

        setupObservers();

        viewModel.requestAirports();
    }

    /**
     * Store airport info to viewModel on click and return to the calling fragment.
     */
    public void setupListeners() {

        binding.lvAirports.setOnItemClickListener((parent, view, position, id) -> {
            switch ((int) requireArguments().get("airport")) {
                case 0:
                    viewModel.setDepartFromAirport(adapter.getItem(position));
                    break;
                case 1:
                    viewModel.setDepartToAirport(adapter.getItem(position));
                    break;
            }

            Navigation.findNavController(requireView()).navigateUp();
        });
    }

    /**
     * Add the list of airports obtained from the server onSuccess;
     * or show a toast to the user onError.
     */
    public void setupObservers() {
        adapter = new ArrayAdapter<>(
                this.getContext(),
                android.R.layout.simple_list_item_1);

        binding.lvAirports.setAdapter(adapter);

        viewModel.airports.observe(getViewLifecycleOwner(), resource -> {
            switch (resource.status) {
                case SUCCESS:
                    adapter.addAll(resource.data);
                    binding.pbLoading.setVisibility(ProgressBar.INVISIBLE);
                    break;
                case ERROR:
                    Toast.makeText(getContext(), resource.message, Toast.LENGTH_SHORT).show();
                    break;
                case LOADING:
                    binding.pbLoading.setVisibility(ProgressBar.VISIBLE);
                    Toast.makeText(getContext(), "loading", Toast.LENGTH_SHORT).show();
                    break;
            }
        });
    }
}