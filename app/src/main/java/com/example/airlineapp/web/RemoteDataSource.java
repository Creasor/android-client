package com.example.airlineapp.web;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.airlineapp.model.Airport;
import com.example.airlineapp.model.Flight;
import com.example.airlineapp.model.FlightRequest;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

import static com.example.airlineapp.web.FlightApi.TOKEN_PATH;

/**
 * This remote data source adapter handles all the logic for invoking
 * Flight Listing App (FLApp) endpoint methods and managing tokens
 * using the OAuth PKCE authorization in conjunction with a WebView.
 * 
 * The {@code @Singleton{} annotation is used to mark the class as
 * Singleton Session Bean, i.e., there's just one instance shared by
 * other components in this app.
 */
@Singleton
public class RemoteDataSource {
    /**
     * Flight Listing App server API instance.
     */
    private final FlightApi api;

    /**
     * Redirect URL returned by OAuth PKCE server during
     * authentication flow.
     */
    public static final String REDIRECT_URI = "client://app/callback";

    /**
     * Emulator URL used to access locally running Flight Listing App
     * (FLApp) server.
     */
    public static final String BASE_URL = "http://10.0.2.2:8081";

    /**
     * Authorization endpoint.
     */
    public static final String AUTHORIZATION_URL = BASE_URL + "/oauth/authorize";

    /**
     * OAuth PKCE client ID.
     */
    public static final String CLIENT_ID = "mobile";

    /**
     * OAuth authorization request parameter to ask for an
     * authorization code.
     */
    public static final String CODE_GRANT_TYPE = "authorization_code";

    /**
     * OAuth authorization request parameter to ask for a refresh
     * token.
     */
    public static final String REFRESH_GRANT_TYPE = "refresh_token";

    /**
     * OAuth authorization request parameter specifying challenge
     * encoding method.
     */
    public static final String CODE_CHALLENGE_METHOD = "S256";

    /**
     * OAuth authorization request parameter requesting
     * offline access required for refresh token support.
     */
    public static final String OFFLINE_SCOPE = "offline_access";

    /**
     * OAuth authorization request parameter requesting a response
     * code.
     */
    public static final String RESPONSE_TYPE = "code";

    /**
     * HTTP request timeouts to prevent infinite blocking of client.
     */
    public static final Duration DEFAULT_TIMEOUT = Duration.ofSeconds(15);

    /**
     * PKCE code challenge.
     */
    private String codeChallenge;

    /**
     * PKCE code verifier.
     */
    private String codeVerifier;

    /**
     * Private random state string to ensure that responses match
     * requests.
     */
    private String state;

    /**
     * Token response from server.
     */
    private TokenResponse tokenResponse;

    /**
     * Token repository for caching token response.
     */
    private final TokenRepository tokenRepository;

    /**
     * RxJava adapter needed to obtain info from the server.
     */
    private final RxJava2CallAdapterFactory rxAdapter = RxJava2CallAdapterFactory
        .createWithScheduler(Schedulers.io());

    /**
     * The constructor initializes the fields.
     * 
     * @param tokenRepository Injected token repository for caching
     * token response
     */
    public RemoteDataSource(TokenRepository tokenRepository) {
        codeVerifier = generateCodeVerifier();
        codeChallenge = generateCodeChallenge(codeVerifier);
        this.tokenRepository = tokenRepository;
        api = buildApi();
    }

    /**
     * Builds an RxJava compatible FlightApi instance that supports
     * token authentication.
     *
     * @return A RxJava compatible FlightApi instance that supports
     * token authentication.
     */
    private FlightApi buildApi() {
        return new Retrofit
            // Use the Builder pattern.
            .Builder()

            // The URL for the FLApp server.
            .baseUrl(BASE_URL)

            // Create an OkHttpClient.
            .client(buildHttpClient())

            // Make this client work with RxJava.
            .addCallAdapterFactory(rxAdapter)

            // Enable Gson encodings.
            .addConverterFactory(GsonConverterFactory
                                 .create(new GsonBuilder()
                                         .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ")
                                         .setLenient()
                                         .create()))

            // Build the Retrofit object.
            .build()

            // Return an instance of the FlightAPI class.
            .create(FlightApi.class);
    }

    /**
     * @return An OkHttpClient that supports token authentication
     */
    private OkHttpClient buildHttpClient() {
        HttpLoggingInterceptor httpLoggingInterceptor =
            new HttpLoggingInterceptor();
        httpLoggingInterceptor
            .setLevel(HttpLoggingInterceptor.Level.BODY);

        // Use the Builder pattern to create an OkHttpClient that
        // supports token authentication.
        return new OkHttpClient
            .Builder()
            .addInterceptor(new TokenInterceptor())
            .authenticator(new TokenAuthenticator())
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(DEFAULT_TIMEOUT)
            .readTimeout(DEFAULT_TIMEOUT)
            .writeTimeout(DEFAULT_TIMEOUT)
            .build();
    }

    /**
     * Finds all known airports.
     *
     * @return A {@link Single} that emits a {@link List} of {@code
     * Airport} objects
     */
    public Single<List<Airport>> getAirports() {
        return api
            // Forward to the API to get a Single that emits the List
            // of Airport objects.
            .getAirports();
    }

    /**
     * Finds all departure dates that have at least one flight running
     * from the departure airport to the arrival airport.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link Single} that emits a {@link List} of all
     * matching dates
     */
    public Single<List<String>> getDepartureDates(String departureAirport,
                                                  String arrivalAirport) {
        return api
            // Forward to the API to get a Single that emits the List
            // of String objects containing departure dates.
            .getDepartureDates(departureAirport, arrivalAirport);
    }

    /**
     * Find all flight(s) that match the given {@code
     * departureAirport} and {@code arrivalAirport} on the given
     * {@code departureDate} and update their price(s) to the desired
     * {@code currency}.
     *
     * @param flightRequest Flight request parameters.
     * @return A {@link List} of matching {@link Flight} objects in
     * the desired currency
     */
    public Single<List<Flight>> findFlights(FlightRequest flightRequest) {
        return api
            // Forward to the API to get a Single that emits the List
            // of matching Flight objects.
            .findFlights(flightRequest.getDepartureAirport(),
                         flightRequest.getArrivalAirport(),
                         flightRequest.getDepartureDate(),
                         flightRequest.getPassengers(),
                         flightRequest.getCurrency());
    }

    /**
     * @return A Completable to subscribe to.
     */
    public Completable logout() {
        return api
            // Log the client out.
            .logout(CLIENT_ID)

            // Clear the cached tokens.
            .doOnComplete(this::clearTokens);
    }

    /**
     * Asynchronously requests a session token from the server.
     *
     * @param code PKCE code received from server authentication response
     * @return A RxJava Single
     */
    public Single<TokenResponse> requestToken(String code) {
        return api
            // Forward to the API to get a Single that emits the
            // TokenResponse.
            .requestToken(CLIENT_ID,
                          CODE_GRANT_TYPE,
                          REDIRECT_URI,
                          code,
                          state,
                          OFFLINE_SCOPE,
                          codeVerifier)

            // Cache the passed token response.
            .map(this::saveTokenResponse);
    }

    /**
     * @return The code verifier to send in a token request.
     */
    private String generateCodeVerifier() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] bytes = new byte[32];
        secureRandom.nextBytes(bytes);
        return Base64
            .getUrlEncoder()
            .withoutPadding()
            .encodeToString(bytes);
    }

    /**
     * @param codeVerifier A code verifier string returned from
     *                     {@link RemoteDataSource#generateCodeVerifier}
     * @return A code challenge that matches the passed codeVerifier.
     */
    private String generateCodeChallenge(String codeVerifier) {
        try {
            MessageDigest sha256Digester = MessageDigest
                .getInstance("SHA-256");

            sha256Digester
                .update(codeVerifier.getBytes(StandardCharsets.US_ASCII));

            byte[] digestBytes = sha256Digester
                .digest();

            return Base64
                .getUrlEncoder()
                .withoutPadding()
                .encodeToString(digestBytes);
        } catch (NoSuchAlgorithmException e) {
            Timber.w(e, "SHA-256 is not supported on this device! Using plain challenge.");
            return codeVerifier;
        }
    }

    /**
     * @return An authorization request URL containing all the required
     * query parameters to begin an OAuth PKCE authentication flow
     */
    public String buildLoginUrl() {
        codeVerifier = generateCodeVerifier();
        codeChallenge = generateCodeChallenge(codeVerifier);
        state = UUID.randomUUID().toString();

        // Return the login URI.
        return Uri
            .parse(AUTHORIZATION_URL)
            .buildUpon()
            .appendQueryParameter("client_id", CLIENT_ID)
            .appendQueryParameter("redirect_uri", REDIRECT_URI)
            .appendQueryParameter("response_type", RESPONSE_TYPE)
            .appendQueryParameter("code_challenge", codeChallenge)
            .appendQueryParameter("code_challenge_method", CODE_CHALLENGE_METHOD)
            .appendQueryParameter("scope", OFFLINE_SCOPE)
            .appendQueryParameter("state", state)
            .build()
            .toString();
    }

    /**
     * @return A randomly generated state string used for response
     * verification
     */
    public String getState() {
        return state;
    }

    /**
     * Clears all cached tokens.
     */
    public void clearTokens() {
        tokenResponse = null;
        tokenRepository.delete();
    }

    /**
     * Caches the passed token response.
     *
     * @param tokenResponse Token response to cache.
     * @return The passed token response.
     */
    private TokenResponse saveTokenResponse(TokenResponse tokenResponse) {
        tokenRepository.save(tokenResponse);
        return tokenResponse;
    }

    /**
     * OkHttp token authentication that is called when the server rejects
     * an expired token. When this occurs, this class will attempt to
     * acquire a new token using the server provided refresh token.
     */
    private class TokenAuthenticator implements Authenticator {
        /**
         * @param route    Same as {@link Authenticator#authenticate(Route, Response)}
         * @param response Same as {@link Authenticator#authenticate(Route, Response)}
         * @return Either a new request containing a refreshed token, or null if
         * token refresh failed.
         */
        @Nullable
        @Override
        public Request authenticate(Route route, @NotNull Response response) {
            if (requestHasAccessToken(response) || isRefreshTokenRequest(response)) {
                // Already failed to authenticate or token refresh has
                // request failed so clear all tokens and fail.
                clearTokens();
                return null;
            }

            final String accessToken = tokenRepository.getAccessToken();

            // Use synchronized block to ensure that only one thread
            // attempts to refresh a stale token.
            synchronized (this) {
                final String newAccessToken = tokenRepository.getAccessToken();
                if (newAccessToken != null && !newAccessToken.equals(accessToken)) {
                    // Access token has already been refreshed in another thread
                    // so return a new request using this newly refreshed token.
                    return newRequestWithAccessToken(response.request(),
                                                     newAccessToken);
                }

                if (tokenRepository.getRefreshToken() == null) {
                    // There is no refresh token available so clear all tokens and fail.
                    clearTokens();
                    return null;
                }

                try {
                    // Synchronously request a refreshed access token from the server.
                    tokenResponse = api
                        .refreshToken(CLIENT_ID,
                                      REFRESH_GRANT_TYPE,
                                      OFFLINE_SCOPE,
                                      tokenRepository.getRefreshToken())
                        .blockingGet();

                    // Cache the token.
                    tokenRepository.save(tokenResponse);

                    // Return a new request containing the new valid token.
                    return newRequestWithAccessToken(
                                                     response.request(),
                                                     tokenResponse.getAccessToken());
                } catch (Throwable throwable) {
                    // Something went wrong, so clear any cached tokens and fail.
                    clearTokens();
                    return null;
                }
            }
        }

        /**
         * Checks if passed response has an access token in the response header.
         *
         * @param response Any response.
         * @return True if an access token is present in the header, false if not.
         */
        private boolean requestHasAccessToken(@NonNull Response response) {
            String header = response.request().header("Authorization");
            return header != null && header.startsWith("Bearer");
        }

        /**
         * Used to identify if passed response is for a token refresh request.
         *
         * @param response Any response.
         * @return True iff passed response is for a token refresh request.
         */
        private boolean isRefreshTokenRequest(@NonNull Response response) {
            return response
                .request()
                .url()
                .toString()
                .endsWith(TOKEN_PATH);
        }

        /**
         * Constructs a new request matching the passed request adding an
         * additional access token to the request header.
         *
         * @param request     Any request.
         * @param accessToken Access token to add to request header.
         * @return A new request with an access token.
         */
        @NonNull
        private Request newRequestWithAccessToken(@NonNull Request request,
                                                  @NonNull String accessToken) {
            return request
                .newBuilder()
                .header("Authorization", "Bearer " + accessToken)
                .build();
        }
    }

    /**
     * An OkHttp {@link  Interceptor} used to inject an access token in all
     * new API request.
     */
    private class TokenInterceptor implements Interceptor {
        /**
         * @param chain Same as {@link Interceptor#intercept(Chain)}
         * @return Same as {@link Interceptor#intercept(Chain)}
         */
        @Override
        public @NotNull Response intercept(@NotNull Interceptor.Chain chain) throws IOException {
            if (tokenRepository.getAccessToken() == null) {
                // No token available so do nothing.
                return chain.proceed(chain.request());
            } else {
                // Token available insert authorization header.
                return chain
                    .proceed((chain
                              .request()
                              .newBuilder()
                              .header("Authorization",
                                      tokenRepository.getAuthorizationHeader())
                              .build()));
            }
        }
    }
}
