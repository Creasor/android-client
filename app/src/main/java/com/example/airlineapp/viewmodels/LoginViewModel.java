package com.example.airlineapp.viewmodels;

import android.app.Application;
import android.webkit.WebResourceRequest;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.airlineapp.App;
import com.example.airlineapp.common.Resource;
import com.example.airlineapp.common.SingleLiveEvent;
import com.example.airlineapp.web.RemoteDataSource;
import com.example.airlineapp.web.TokenResponse;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.example.airlineapp.web.RemoteDataSource.REDIRECT_URI;


/**
 * View model for handling OAuth PKCE login flow along with a WebView Fragment.
 */
public class LoginViewModel extends AndroidViewModel {

    private final SingleLiveEvent<Resource<String>> loginResult = new SingleLiveEvent<>();
    private final MutableLiveData<String> urlFeed = new MutableLiveData<>();

    private Disposable requestTokenSubscription;

    /**
     * Injected data source used to make API calls to remote server.
     */
    @Inject
    RemoteDataSource dataSource;

    public LoginViewModel(Application app) {
        super(app);
        ((App) app).getAppComponent().inject(this);
    }

    /**
     * @return A login URL from remote data source.
     */
    public String buildLoginUrl() {
        return dataSource.buildLoginUrl();
    }

    /**
     * Helper method called from WebView fragment to look for a server
     * redirect request that contains a OAuth PKCE code that, when found,
     * triggers a server API request for a token.
     *
     * @param request WebView request
     * @return True if token request was processed, false if not.
     */
    public boolean handleUrlRedirect(@NonNull WebResourceRequest request) {
        if (request.getUrl().toString().startsWith(REDIRECT_URI)) {
            // Check unique state parameter to prevent CSRF attacks.
            String code = request.getUrl().getQueryParameter("code");
            if (code != null) {
                System.out.println("OAuth: authorization code is " + code);
                String state = request.getUrl().getQueryParameter("state");
                if (dataSource.getState().equals(state)) {
                    requestToken(code);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Helper method to call remote data source to perform token API request.
     *
     * @param code Code received from server in WebView redirect request.
     */
    public void requestToken(String code) {
        if (requestTokenSubscription != null && !requestTokenSubscription.isDisposed()) {
            requestTokenSubscription.dispose();
        }

        // Request token asynchronously on a background thread.
        requestTokenSubscription = dataSource
            // Asynchronously requests a session token from the
            // server.
            .requestToken(code)

            // Run this request in the I/O thread pool.
            .subscribeOn(Schedulers.io())

            // Terminal operator handles token success or failure by
            // posting the corresponding notifications to the
            // fragment.
            .subscribe(this::tokenSuccess, this::tokenError);
    }

    /**
     * Handle token success by posting a success notification to
     * fragment.
     *
     * @param tokenResponse Not used.
     */
    private void tokenSuccess(TokenResponse tokenResponse) {
        loginResult.postValue(Resource.success(""));
    }

    /**
     * Handle token error by posting a value to the fragment.
     *
     * @param throwable Exception causing the failure.
     */
    private void tokenError(Throwable throwable) {
        loginResult.postValue(Resource.error(throwable.getMessage(), null));
    }

    /**
     * @return The current login result.
     */
    public LiveData<Resource<String>> getLoginResult() {
        return loginResult;
    }

    /**
     * @return The URL live data feed.
     */
    public LiveData<String> getUrlFeed() {
        return urlFeed;
    }
}
