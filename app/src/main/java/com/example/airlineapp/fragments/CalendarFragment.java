package com.example.airlineapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.airlineapp.R;
import com.example.airlineapp.databinding.CalendarFragmentBinding;
import com.example.airlineapp.viewmodels.MainViewModel;
import com.squareup.timessquare.CalendarPickerView.OnDateSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

import timber.log.Timber;

import static com.squareup.timessquare.CalendarPickerView.SelectionMode.RANGE;

public class CalendarFragment extends Fragment {

    private MainViewModel viewModel;
    private CalendarFragmentBinding binding;
    List<Date> dates;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.calendar_fragment,
                container,
                false);
        // set the ViewModel for the calendar fragment
        viewModel = (new ViewModelProvider(requireActivity())).get(MainViewModel.class);
        binding.setViewModel(viewModel);
        binding.setFragment(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);
        binding.calendar.init(new Date(), nextYear.getTime());

        setupObservers();

        // Request dates from server (hardcoded for testing).
        viewModel.requestDepartureDates("1", "2");
    }

    /**
     * Return to the flights fragment when finish selecting dates.
     */
    public void onDoneClicked() {
        Navigation.findNavController(requireView()).navigateUp();
    }

    /**
     * Get available dates from the server.
     */
    private void setupObservers() {
        viewModel.dates.observe(getViewLifecycleOwner(), resource -> {
            switch (resource.status) {
                case SUCCESS:
                    if (resource.data != null && !resource.data.isEmpty()) {
                        initializeCalendar(resource.data);
                    } else {
                        Toast.makeText(getContext(),
                                "No available flight dates.",
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
                case ERROR:
                    Toast.makeText(getContext(), resource.message, Toast.LENGTH_SHORT).show();
                    break;
                case LOADING:
                    Toast.makeText(getContext(), "loading", Toast.LENGTH_SHORT).show();
                    break;
            }
        });
    }

    private void initializeCalendar(List<String> dateStrings) {
        dates = dateStrings
                .stream()
                .map(dateString -> {
                    try {
                        return new SimpleDateFormat(
                                "yyyy-MM-dd",
                                Locale.getDefault()).parse(dateString);
                    } catch (ParseException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .sorted()
                .collect(Collectors.toList());


        List<Date> curDates = new ArrayList<>();
        if (viewModel.getDepartureDate().getValue() != null) {
            curDates.add(viewModel.getDepartureDate().getValue());
        }
        if (viewModel.getReturnDate().getValue() != null) {
            curDates.add(viewModel.getReturnDate().getValue());
        }

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        // Only allow valid dates to be selected.
        binding.calendar.setDateSelectableFilter(this::isDateSelectable);

        // Create the calendar.
        binding.calendar
                .init(dates.get(0), nextYear.getTime())
                .withHighlightedDates(dates)
                .inMode(RANGE)
                .withSelectedDates(curDates);

        binding.calendar.scrollToDate(dates.get(0));

        // Update depart and return dates on click.
        binding.calendar.setOnDateSelectedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                List<Date> selectedDates = binding.calendar.getSelectedDates();
                viewModel.setDepartureDate(selectedDates.get(0));
                viewModel.setReturnDate(selectedDates.get(selectedDates.size() - 1));
            }

            @Override
            public void onDateUnselected(Date date) {
                Timber.d("date unselected");
            }
        });
    }

    private boolean isDateSelectable(Date date) {
        return !(date.before(dates.get(0)) || date.after(dates.get(dates.size() - 1)));
    }
}