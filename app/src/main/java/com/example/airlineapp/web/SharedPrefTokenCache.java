package com.example.airlineapp.web;

import android.content.SharedPreferences;

import com.google.gson.Gson;

/**
 * A shared prefence implementation of a TokenCache interface.
 */
public class SharedPrefTokenCache implements TokenCache {
    /**
     * Shared preference key.
     */
    private static final String TOKEN_PREF = "TokenResponse";

    /**
     * Injected SharedPreference instance.
     */
    private final SharedPreferences sharedPreferences;

    /**
     * Constructor requiring an injected SharedPreference instance.
     * @param sharedPreferences Injected SharedPreference instance.
     */
    public SharedPrefTokenCache(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    /**
     * @return True iff a token exists in this cache.
     */
    @Override
    public boolean exists() {
        return get() != null;
    }

    /**
     * @return A previously cached token.
     */
    @Override
    public TokenResponse get() {
        String json = sharedPreferences.getString(TOKEN_PREF, null);
        return json != null ? new Gson().fromJson(json, TokenResponse.class) : null;
    }

    /**
     * Saves the passed tokenResonse in the cache.
     * @param tokenResponse A token response object.
     */
    @Override
    public void save(TokenResponse tokenResponse) {
        sharedPreferences
            .edit()
            .putString(TOKEN_PREF, new Gson().toJson(tokenResponse))
            .apply();
    }

    /**
     * Deletes any cached token.
     */
    @Override
    public void delete() {
        sharedPreferences.edit().remove(TOKEN_PREF).apply();
    }
}
