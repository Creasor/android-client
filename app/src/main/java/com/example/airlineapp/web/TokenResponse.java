package com.example.airlineapp.web;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * A token response DTO as sent by an OAuth PKCE server.
 */
public class TokenResponse {
    @SerializedName("access_token")
    String accessToken = "";
    @SerializedName("refresh_token")
    String refreshToken = "";
    @SerializedName("token_type")
    String tokenType = "";
    @SerializedName("expires_in")
    String expiresInSeconds = "";
    @SerializedName("scope")
    String scopes = "";

    @NonNull
    public String getAccessToken() {
        return accessToken;
    }

    @NonNull
    public String getRefreshToken() {
        return refreshToken;
    }

    @NonNull
    public String getTokenType() {
        return tokenType;
    }

    @NonNull
    public String getExpiresInSeconds() {
        return expiresInSeconds;
    }

    @NonNull
    public String getScopes() {
        return scopes;
    }
}