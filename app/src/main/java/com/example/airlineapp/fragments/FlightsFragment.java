package com.example.airlineapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.airlineapp.R;
import com.example.airlineapp.databinding.FlightsFragmentBinding;
import com.example.airlineapp.viewmodels.MainViewModel;

import org.jetbrains.annotations.NotNull;

public class FlightsFragment extends BaseFragment {

    private FlightsFragmentBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.flights_fragment,
                container,
                false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.setViewModel(new ViewModelProvider(requireActivity()).get(MainViewModel.class));
        binding.setLifecycleOwner(this);
        binding.setFragment(this);
    }

    @Override
    public void onCreateOptionsMenu(
            @NonNull @NotNull Menu menu,
            @NonNull @NotNull MenuInflater inflater
    ) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull @NotNull MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            viewModel.logout();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void showCalendarPicker() {
        Navigation.findNavController(requireView())
                .navigate(R.id.action_flightsFragment_to_calendarFragment);
    }

    public void showSearchResult() {
        Navigation.findNavController(requireView())
                .navigate(R.id.action_flightsFragment_to_departFragment);
    }

    public void showAirportList(int airport) {
        Navigation.findNavController(requireView()).navigate(
                FlightsFragmentDirections.actionFlightsFragmentToAirportsFragment().setAirport(airport)
        );
    }
}