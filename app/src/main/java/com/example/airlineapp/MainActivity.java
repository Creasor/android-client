package com.example.airlineapp;

import android.os.Bundle;
import android.webkit.CookieManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.airlineapp.viewmodels.MainViewModel;

import static java.util.Objects.requireNonNull;

public class MainActivity extends AppCompatActivity {

    private final FragmentManager fragmentManager = getSupportFragmentManager();
    private AppBarConfiguration appBarConfiguration;
    private NavController navController;
    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        // Establish a single view model used for all fragments.
        viewModel = (new ViewModelProvider(this)).get(MainViewModel.class);

        setupNavigation();
        setupObservers();
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, appBarConfiguration);
    }

    private void setupNavigation() {
        // Setup navigation controller with navigate up support.
        Fragment navFragment = requireNonNull(
                fragmentManager.findFragmentById(R.id.nav_host_fragment));
        navController = ((NavHostFragment) navFragment).getNavController();

        NavigationUI.setupActionBarWithNavController(this, navController);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();

        navController.addOnDestinationChangedListener((controller, destination, args) -> {
            boolean showButton = showUpButton(destination.getId());
            requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(showButton);
            getSupportActionBar().setDisplayHomeAsUpEnabled(showButton);
        });
    }

    private void setupObservers() {
        viewModel.getAuthenticate().observe(this,
                unused -> {
                    // Clear WebView cookies.
                    CookieManager.getInstance().removeAllCookies(result -> {});

                    // Re-authenticate.
                    navController.navigate(R.id.action_login_fragment);
                });
    }

    private boolean showUpButton(int id) {
        return id != R.id.loginFragment && id != R.id.flightsFragment;
    }
}